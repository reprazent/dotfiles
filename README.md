## A place to keep my dotfiles

### Usage

Clone this repo and run `install.sh` to symlink dotfiles to the homedir

### What's here

#### `.gemrc`

Manly skips installing documentation for gems using `--no-rdoc` and `--no-ri`

#### Git configuration

This config forces me to set an email per repo. Making sure I use the correct one per repo

It sets a global gitignore, meaning files that will be ignored in every repo.

Contains aliases

#### `.zshrc`

Basic modifications to my `$PATH`. Sets my desired editor. Some aliasses
