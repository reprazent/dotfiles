#!/usr/bin/env bash

make_symlinks() {
	cat <<EOF
----- Symlinks -----
.gemrc
.gitconfig
.gitignore_global
.zshrc
.profile
.bashrc
--------------------

EOF
	current="$(pwd)"
	ln -fs "$current/gemrc" "$HOME/.gemrc"
	ln -fs "$current/gitconfig" "$HOME/.gitconfig"
	ln -fs "$current/gitignore_global" "$HOME/.gitignore_global"
	ln -fs "$current/config/profile" "$HOME/.profile"
	ln -fs "$current/config/bash_profile" "$HOME/.bash_profile"
	ln -fs "$current/config/shellrc" "$HOME/.bashrc"
	ln -fs "$current/config/shellrc" "$HOME/.zshrc"
	ln -fs "$current/config/mise.toml" "$HOME/.config/mise/config.toml"
	ln -fs "$current/bash-it" "$HOME/.bash-it"

	./scripts/symlink-files.sh "$current/local/bin" "$HOME/.local/bin"
	./scripts/symlink-files.sh "$current/local/share" "$HOME/.local/share"
}

install() {
	make_symlinks
}

install
