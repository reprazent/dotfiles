#!/usr/bin/env ruby
# frozen_string_literal: true

require 'net/http'
require 'json'

TOKEN = ENV.fetch('GITLAB_TOKEN')
RENOVATE_USERNAMES = %w[glrenovatebot manifest-auto-sync gitlab-bot gitlab-release-tools-bot].freeze

def ignorable_username(username)
  RENOVATE_USERNAMES.include?(username) || username.match(/(project|group)_\d+_bot_.*/)
end

def mark_as_done(todo, conn)
  puts "Done: #{todo['target_url']}"
  result = conn.post("https://gitlab.com/api/v4/todos/#{todo['id']}/mark_as_done?private_token=#{TOKEN}", '{}')
  raise result.inspect unless result.is_a?(Net::HTTPSuccess)

  true
end

def handle_todo(todo, conn)
  author_username = todo['author']['username']
  target_state = todo['target']['state']

  if ignorable_username(author_username) && todo['action_name'] == 'review_requested'
    return mark_as_done(todo, conn)
  end

  if ignorable_username(author_username) && ["merged", "closed"].include?(target_state)
    return mark_as_done(todo, conn)
  end

  if ignorable_username(author_username) &&
     todo['action_name'] == 'mentioned'
    return mark_as_done(todo, conn)
  end

  # Review requests but no longer assigned
  return false unless todo['action_name'] == 'review_requested'

  assigned_usernames = todo['target']['reviewers'].map { |reviewer| reviewer['username'] }

  mark_as_done(todo, conn) unless assigned_usernames.include?('reprazent')
rescue StandardError
  puts "failed to handle todo: #{todo.inspect}"
end

def handle_page(result, conn)
  todos = JSON.parse(result.body)
  puts "--- Handling page: #{result.header['x-page']} (#{todos.size} todos)"
  dones, = todos.partition do |todo|
    handle_todo(todo, conn)
  end

  dones.size.positive?
rescue StandardError
  puts 'failed to handle todos for page'
end

Net::HTTP.start('gitlab.com', 443, use_ssl: true) do |conn|
  result = conn.get("https://gitlab.com/api/v4/todos?private_token=#{TOKEN}")

  loop do
    current_page = result.header['x-page'].to_i
    next_page = result.header['x-next-page'].to_i

    redo_page = handle_page(result, conn)

    current_page = next_page unless redo_page
    break if current_page.zero?

    result = conn.get("https://gitlab.com/api/v4/todos?private_token=#{TOKEN}&page=#{current_page}")
  end
end
