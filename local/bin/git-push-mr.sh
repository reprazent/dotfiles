#!/bin/bash

# Get current branch name
current_branch=$(git branch --show-current)

# Get the default branch (usually main or master)
default_branch=$(git symbolic-ref refs/remotes/origin/HEAD | gsed 's@^refs/remotes/origin/@@')

# Find the commit where the current branch diverges from the default branch
merge_base=$(git merge-base "$default_branch" "$current_branch")

# Get the first commit message after diverging (oldest commit in the branch)
first_commit_title=$(git log --format=%s "${merge_base}..${current_branch}" | tail -n 1)

# Get all commit messages since diverging, newest first
commit_messages=$(git log --reverse --format="**%s**\\n\\n%b" ${merge_base}..${current_branch} | gsed -z 's/\n/\\n/g')

# Push with merge request options, passing through any additional arguments ($@)
git push --set-upstream origin ${current_branch} \
	-o merge_request.create \
	-o merge_request.title="${first_commit_title}" \
	-o merge_request.description="${commit_messages}" \
	"$@"
