#!/usr/bin/env ruby
# frozen_string_literal: true

require 'net/http'
require 'json'
require 'uri'
require 'pry'

TOKEN = ENV.fetch('GITLAB_TOKEN')

def uri(path)
  URI.join('https://gitlab.com/api/v4/', path)
end

def auth(request)
  request['private-token'] = TOKEN
end

def get(conn, path, params)
  url = uri(path)
  url.query = URI.encode_www_form(params)

  request = Net::HTTP::Get.new(url.request_uri)
  auth(request)

  puts "GET #{url}"
  conn.request(request)
end

def post(conn, path, params)
  url = uri(path)

  request = Net::HTTP::Post.new(url.request_uri)
  auth(request)
  request.body = params.to_json

  puts "POST #{url}: #{request.body}"
  conn.request(request)
end

def put(conn, path, params)
  url = uri(path)

  request = Net::HTTP::Put.new(url.request_uri)
  auth(request)
  request.body = params.to_json

  puts "PUT #{url}"
  conn.request(request)
end

def safe_project?(web_url)
  safe_projects = [
    'gitlab-com/gl-infra/tamland', # These need to be merged to trackers in a version bump
    'gitlab-com/gl-infra/capacity-planning-trackers/tamland-staging', # It's staging
    'gitlab-com/gl-infra/capacity-planning-trackers/gitlab-com', # we get shouted at on Slack if this fails
    'gitlab-com/gl-infra/observability/tenant-observability/config-manager', # This needs a version bump later in tenant-observability-stack
    'gitlab-com/gl-infra/observability/database-growth-analysis' # reports will fail if something is off
  ]
  safe_urls = safe_projects.map { |path| "https://gitlab.com/#{path}" }
  safe_urls.any? { |url| web_url.start_with?(url) }
end

def safe_dependency?(title)
  # These are dependencies we build ourselves, those already got careful review from talented people
  safe_deps = [
    'https://gitlab.com/gitlab-com/gl-infra/common-template-copier.git',
    'gitlab-com/gl-infra/jsonnet-tool'
  ]

  safe_deps.any? { |d| title.downcase.include?("update dependency #{d}") }
end

def safe_renovate_mr?(mr)
  unless mr['labels'].include?('maintenance::dependency') &&
         mr['author']['username'].start_with?("project_#{mr['project_id']}_bot")
    return false
  end

  mr['labels'].include?('dependency::dev') || safe_project?(mr['web_url']) || safe_dependency?(mr['title'])
end

def i_approved?(approvals_body)
  approvals_body['approved_by']&.any? { |approver| approver.dig('user', 'username').to_s.downcase == 'reprazent' }
end

def approve_mr(conn, mr)
  approvals_result = get(conn, "projects/#{mr['project_id']}/merge_requests/#{mr['iid']}/approvals", {})
  approvals_body = JSON.parse(approvals_result.body)
  return true if i_approved?(approvals_body)

  approval_result = post(conn, "projects/#{mr['project_id']}/merge_requests/#{mr['iid']}/approve", {})
  approval_body = JSON.parse(approval_result.body)
  i_approved?(approval_body)
end

def merging?(mr)
  mr['merge_when_pipeline_succeeds'] && mr['detailed_merge_status'] == 'ci_still_running'
end

def merge_mr(conn, mr)
  return false if merging?(mr)

  if approve_mr(conn, mr)
    # try a merge train first, if it's not enabled for the project, we'll get a 4xx
    merge_train_result = post(conn, "projects/#{mr['project_id']}/mer56ge_trains/merge_requests/#{mr['iid']}",
                              { merge_when_pipeline_succeeds: true })
    return true if merge_train_result.code == '200'

    merge_result = put(conn, "projects/#{mr['project_id']}/merge_requests/#{mr['iid']}/merge",
                       { merge_when_pipeline_succeeds: true })
    merge_result.code == '200'
  else
    puts "could not approve #{mr['web_url']}"
    false
  end
end

def process_page(conn, result)
  mrs = JSON.parse(result.body)
  puts "--- Handling page: #{result.header['x-page']} (#{mrs.size} mrs)"

  merged = []
  mrs.each do |mr|
    next unless safe_renovate_mr?(mr)

    mr_details = get(conn, "projects/#{mr['project_id']}/merge_requests/#{mr['iid']}", {})
    mr = JSON.parse(mr_details.body)
    puts "Merging #{mr['web_url']}"

    if merge_mr(conn, mr)
      merged << mr
    else
      puts "could not merge #{mr['web_url']}"
    end
  end

  merged.size.positive?
end

Net::HTTP.start('gitlab.com', 443, use_ssl: true) do |conn|
  current_page = 1
  loop do
    result = get(
      conn, 'merge_requests',
      { 'state' => 'opened', 'reviewer_username' => 'reprazent', 'scope' => 'all', 'page' => current_page, 'labels' => 'maintenance::dependency' }
    )

    process_page(conn, result)

    current_page = result.header['x-next-page'].to_i

    break if current_page.zero?
  end
end
