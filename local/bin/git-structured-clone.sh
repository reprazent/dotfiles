#!/usr/bin/env bash

if [ -z "$1" ]; then
	echo "Usage: git-structured-clone.sh <git-url>" \
		"this will clone repo in '~/repos/<forge>/<namespace>/<project>'"
	exit 1
fi

url="$1"

# Handle SSH URLs (git@host:path.git)
if [[ $url =~ ^git@([^:]+):~?(.+)$ ]]; then
    forge=${BASH_REMATCH[1]}
    path=${BASH_REMATCH[2]}
# Handle HTTPS URLs (https://host/path.git)
elif [[ $url =~ ^https?://([^/]+)/~?(.+)$ ]]; then
    forge=${BASH_REMATCH[1]}
    path=${BASH_REMATCH[2]}
else
    echo "Invalid Git URL format. Supported formats:"
    echo "  - SSH:   git@host:path[.git]"
    echo "  - SSH:   git@host:~path"
    echo "  - HTTPS: https://host/path[.git]"
    echo "  - HTTPS: https://host/~path"
    exit 1
fi

# Remove .git suffix if present
path=${path%.git}

target_dir="$HOME/repos/$forge/$path"
mkdir -p "$target_dir"
git clone "$url" "$target_dir"
echo "Cloned into $target_dir"
