#!/usr/bin/env bash

export _Z_DATA="$HOME/.local/state/z/frecency"
mkdir -p $(dirname $_Z_DATA)
dir=$(dirname $(realpath "${BASH_SOURCE[0]}"))
. $dir/z.sh
