#!/usr/bin/env bash

# Usage: ./symlink_files.sh <source_dir> <target_dir>

if [ "$#" -ne 2 ]; then
	echo "Usage: $0 <source_dir> <target_dir>"
	exit 1
fi

source_dir="$1"
target_dir="$2"

# Check if source directory exists
if [ ! -d "$source_dir" ]; then
	echo "Error: Source directory '$source_dir' does not exist"
	exit 1
fi

# Create target directory if it doesn't exist
mkdir -p "$target_dir"

# Loop through top-level files and directories in source directory
find "$source_dir" -maxdepth 1 ! -path "$source_dir" -print0 | while IFS= read -r -d $'\0' source_item; do
	# Get the relative path from source_dir
	rel_path="${source_item#$source_dir/}"

	# Create the symbolic link
	target_item="$target_dir/$rel_path"

	if [ -e "$target_item" ]; then
		echo "Warning: '$target_item' already exists, skipping..."
	else
		ln -s "$source_item" "$target_item"
		echo "Created symlink: $target_item -> $source_item"
	fi
done
